# Python challenge: Stock Quotes Chatroom

A simple financial chatroom app developed as a submission for a [code challenge](https://drive.google.com/file/d/155bPu-sSQ4vDbG2VuyNgSnD7O3_nIzGG/view?usp=sharing).
The project was initially bootstrapped with a
[FastApi+React boilerplate](https://github.com/Buuntu/fastapi-react) and later 
modified to fix a couple of deployment bugs and to implement the challenge.

## Components used within Docker

- **FastAPI** with Python 3.8 (3.9.2 for local development)
- **React 16** with Typescript, Redux, and react-router
- Postgresql
- SqlAlchemy with Alembic for migrations
- Pytest for backend tests
- Jest for frontend tests
- Docker compose
- Nginx as a reverse proxy to allow backend and frontend on the same port

## Development and execution

The only dependencies for this project are docker and docker-compose.

### Quick Start

In the backend directory, create an .env file based on the provided .env_example.

Starting the project with hot-reloading enabled
(first time will take a while):

```bash
docker-compose up -d
```

To run alembic migrations for each model:

```bash
docker-compose run --rm backend alembic upgrade head
```

Lastly, navigate to http://localhost:8000

_Note: If you see an Nginx error at first with a `502: Bad Gateway` page, you may have to wait for webpack to build the development server (the nginx container builds much more quickly)._

Auto-generated docs for the api will be accessible at (but not really necessary)
http://localhost:8000/api/docs

### Implementation for the challenge

The bot responsible for retrieving stock quotes communicates with the
main backend service through a redis pubsub queue. The log of messages for the
chatroom is kept within a regular redis queue. The main endpoints for this challenge
can be found at the chatroom app. Auth is a basic JWT TOKEN OAUTH2 impl through
dependency inj, characteristic of FastAPI. The current config for polling the queue
for new messages is slow, so you might want to change that through the POLL_INTERVAL
var (frontend; websockets didn't work without TLS). One thing to note is that the frontend hasn't been worked much upon 
and as a matter of fact, auto scroll for the chatbox hasn't been implemented either.
Due to time constraints, auth tokens were saved on localStorage instead of httpCookies or a better
alternative, and the secret key used for encoding JWT tokens isn't read from a private .env file but was left hardcoded.

### Rebuilding containers:

```
docker-compose build
```

### Restarting containers:

```
docker-compose restart
```

### Bringing containers down:

```
docker-compose down
```

## Database Migrations

Migrations are run using alembic. To run all migrations:

```
docker-compose run --rm backend alembic upgrade head
```

To create a new migration, do:

```
alembic revision -m "create users table"
```

and fill in `upgrade` and `downgrade` methods. For more information see
[Alembic's official documentation](https://alembic.sqlalchemy.org/en/latest/tutorial.html#create-a-migration-script).

## Testing (Not used for the challenge)

There is a helper script for both frontend and backend tests:

```
./scripts/test.sh
```

### Backend Tests

```
docker-compose run backend pytest
```

any arguments to pytest can also be passed after this command

### Frontend Tests

```
cd frontend
npm install
npm test
```
or

```
docker-compose run frontend test
```

This is the same as running npm test from within the frontend directory

## Logging (Not used for the challenge)

```
docker-compose logs
```

Or for a specific service:

```
docker-compose logs -f name_of_service # frontend|backend|db
```

## Initial project layout from boilerplate 

```
backend
└── app
    ├── alembic
    │   └── versions # where migrations are located
    ├── api
    │   └── api_v1
    │       └── endpoints
    ├── core    # config
    ├── db      # db models
    ├── tests   # pytest
    └── main.py # entrypoint to backend

frontend
└── public
└── src
    ├── components
    │   └── Home.tsx
    ├── config
    │   └── index.tsx   # constants
    ├── __tests__
    │   └── test_home.tsx
    ├── index.tsx   # entrypoint
    └── App.tsx     # handles routing
```
