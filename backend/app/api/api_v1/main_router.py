from fastapi import APIRouter, Depends
from app.core.auth import get_current_active_user, get_current_user
from starlette.requests import Request

from .routers import users, chatroom

api_v1_router = APIRouter(prefix="/api/v1", tags=["v1"])
api_v1_router.include_router(
    users.users_router,
    tags=["users"],
    dependencies=[Depends(get_current_active_user)], # User authentication
)
api_v1_router.include_router(
    chatroom.chatroom_router,
    tags=["chatroom"],
    dependencies=[Depends(get_current_user)] # User authentication
)

@api_v1_router.get("/")
async def root(request: Request):
    url_list = [
        {'path': route.path, 'name': route.name}
        for route in request.app.routes
    ]
    return url_list
