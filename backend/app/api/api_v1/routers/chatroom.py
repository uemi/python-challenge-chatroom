from fastapi import APIRouter#, WebSocket

from app.chatroom.listeners import quote_listener_generator, redirect_msg, msg_cache, \
    quote_processor, data_regex
from app.chatroom.schemas import Message

chatroom_router = APIRouter(prefix="/chatroom")

# Since TLS is not configured I can't use  websockets
# @chatroom_router.websocket("/live-chat")
# async def live_chat_endpoint(chat_ws: WebSocket):
@chatroom_router.post("/send_msg")
async def live_chat_endpoint(msg: Message):
    redirect_msg(msg)
    return {"message": "successfully published"}


# Since I'm not using Websockets, polling will be used instead
@chatroom_router.get("/retrieve_msgs")
async def live_msgs_endpoint():
    next_queued_quote = next(quote_listener_generator)

    if next_queued_quote:
        msgs = msg_cache(quote_processor(next_queued_quote["data"].decode()))
    else:
        msgs = msg_cache(None)

    msg_datas = []
    for msg in msgs:
        data_match = data_regex.match(msg.decode())
        if data_match:
            msg_datas.append(data_match.group(1))
        else:
            msg_datas.append(msg)
    return {"messages": msg_datas}
