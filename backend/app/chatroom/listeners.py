import re
import json

from app.chatroom.schemas import Message
from app.core.utils import use_redis, as_listener_generator, use_redis_queue, RedisQueue


quote_regex = re.compile(r"/stock=(.+)")
data_regex = re.compile(r".*data.*?b'(.+?)'")


@as_listener_generator
@use_redis(is_async=False)
def quote_listener_generator(redis):
    p = redis.pubsub(ignore_subscribe_messages=True)
    p.subscribe(["quotes-channel"])
    return p

def quote_processor(quote):
    print(type(quote))
    try:
        code, val = quote.split(',')
        return json.dumps({"user": "quotes-bot", "text": f"{code} quote is {val} per share."})
    except:
        return quote

@use_redis_queue("msg", "queue")
@use_redis(is_async=False)
def redirect_msg(msg: Message, redis_queue: RedisQueue, redis):
    match_quote_req = quote_regex.match(msg.text)
    if match_quote_req:
        quote = match_quote_req.group(1)
        redis.publish("stock-msgs-channel", quote)
    else:
        redis_queue.put(msg.json())


@use_redis_queue("msg", "queue")
def msg_cache(new_message, redis_queue: RedisQueue):
    if new_message is not None:
        redis_queue.put(new_message)

    if redis_queue.qsize() > 60:
        redis_queue.pop(block=False)

    return redis_queue.slice(0, min(60, redis_queue.qsize()))
