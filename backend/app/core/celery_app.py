from celery import Celery

celery_app = Celery("main", broker="redis://redis:6379/0")

celery_app.conf.task_routes = {
    "app.tasks.*": ["quotes"],
    "quotetasks.*": ["quotes"]
}
