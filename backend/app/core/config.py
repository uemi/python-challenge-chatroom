# import os
from pydantic import BaseSettings, Field
from functools import lru_cache
from typing import Optional

PROJECT_NAME = "python-challenge-chatroom"

# SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL")

API_V1_STR = "/api/v1"

class Env(BaseSettings):
    database_url: str
    host: Optional[str] = "0.0.0.0"
    port: Optional[int] = 8888
    is_dev: Optional[int] = Field(default=1, lt=2, gt=-1)

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"

@lru_cache()
def get_settings():
    return Env()
