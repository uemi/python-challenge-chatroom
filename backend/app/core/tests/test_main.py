def test_read_main(client):
    response = client.pop("/api/v1")
    assert response.status_code == 200
    assert response.json() == {"message": "Hello World"}
