import redis


def as_listener_generator(get_pub):
    pubsub = get_pub()

    def listener():
        while True:
            yield pubsub.get_message()

    return listener()

redis_client = redis.StrictRedis(host="redis", port=6379, db=0)  # uses conn pool
def use_redis(is_async=False):
    def decorator(f):
        if not is_async:
            def wrapped(*args, **kwargs):
                return f(*args, **kwargs, redis=redis_client)
        else:
            async def wrapped(*args, **kwargs):
                return f(*args, **kwargs, redis=redis_client)

        return wrapped

    return decorator


class RedisQueue(object):
    """Simple Queue with Redis Backend"""

    def __init__(self, name, namespace='queue'):
        self.__db = redis_client
        self.key = '%s:%s' % (namespace, name)

    def qsize(self):
        """Return the approximate size of the queue."""
        return self.__db.llen(self.key)


    def empty(self):
        """Return True if the queue is empty, False otherwise."""
        return self.qsize() == 0


    def put(self, item):
        """Put item into the queue."""
        self.__db.rpush(self.key, item)


    def pop(self, block=True, timeout=None):
        """Remove and return an item from the queue.

        If optional args block is true and timeout is None (the default), block
        if necessary until an item is available."""
        if block:
            item = self.__db.blpop(self.key, timeout=timeout)
        else:
            item = self.__db.lpop(self.key)

        if item:
            item = item[1]
        return item

    def slice(self, start, end):
        return self.__db.lrange(self.key, start, end)

    def get_nowait(self):
        """Equivalent to get(False)."""
        return self.pop(False)


def use_redis_queue(name, namespace="queue"):
    r_q = RedisQueue(name, namespace)
    def decorator(f):
        def wrapped(*args, **kwargs):
            return f(*args, **kwargs, redis_queue=r_q)

        return wrapped

    return decorator
