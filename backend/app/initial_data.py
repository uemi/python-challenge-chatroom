#!/usr/bin/env python3

from app.users.crud import create_user
from app.users.schemas import UserCreate
from app.users.session import SessionLocal


def init() -> None:
    db = SessionLocal()

    create_user(
        db,
        UserCreate(
            email="admin@python-challenge-chatroom.com",
            password="password1!",
            is_active=True,
            is_superuser=True,
        ),
    )


if __name__ == "__main__":
    print("Creating superuser admin@python-challenge-chatroom.com")
    init()
    print("Superuser created")
