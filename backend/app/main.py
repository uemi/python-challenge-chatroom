import uvicorn
from fastapi import FastAPI
from starlette.requests import Request

from app.api.api_v1.main_router import api_v1_router
from app.api.api_v1.routers.auth import auth_router
from app.core import config
from app.users.session import SessionLocal
from fastapi.middleware.cors import CORSMiddleware


app = FastAPI(
    title=config.PROJECT_NAME, docs_url="/api/docs", openapi_url="/api"
)

origins = [
    "http://localhost",
    "http://localhost:8080",
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)


@app.middleware("http")
async def db_session_middleware(request: Request, call_next):
    request.state.db = SessionLocal()
    response = await call_next(request)
    request.state.db.close()
    return response




# Routers
app.include_router(api_v1_router)
app.include_router(auth_router, prefix="/api", tags=["auth"])

if __name__ == "__main__":
    env_vars = config.get_settings()
    host = env_vars.host
    is_dev = bool(env_vars.is_dev)
    port = env_vars.port

    uvicorn.run("main:app", host=host, reload=is_dev, port=port)
