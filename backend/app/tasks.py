# import re
# import json
#
# from app.core.celery_app import celery_app
# from app.core.utils import use_redis

# quote_regex = re.compile(r"/stock=(.+)")
#
# @celery_app.task()
# @use_redis(is_async=False)
# def return_quote(quote: dict, redis) -> str:
#     # Keys: Symbol, Date, Time, Open, High, Low, Close, Volume
#     formatted_quote = f"{quote['Symbol']} quote is ${quote['Close']} per share"
#     msg = {"user": "quotes-bot", "text": formatted_quote}
#
#     redis.publish("quotes-channel", json.dumps(msg))
#     return "Done queueing quote"
#
#
# @celery_app.task()
# @use_redis(is_async=False)
# def queue_msg(msg: str, redis):
#     redis.publish("msgs-channel", msg)
#     return "Done queueing msg"
#
#
# @celery_app.task()
# def process_msg(msg: str):
#     msg = json.loads(json.loads(msg))
#     match_quote_req = quote_regex.match(msg["text"])
#     if match_quote_req:
#         quote = match_quote_req.group(1)
#         celery_app.send_task("quotetasks.get_quote", (quote,))
#     else:
#         queue_msg.delay(json.dumps(msg))
#
#     return "Done processing msg"
