import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Fab from '@material-ui/core/Fab';
import SendIcon from '@material-ui/icons/Send';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  chatSection: {
    width: "700px",
    height: '85vh'
  },
  headBG: {
    backgroundColor: '#e0e0e0'
  },
  borderRight500: {
    borderRight: '1px solid #e0e0e0'
  },
  messageArea: {
    height: '70vh',
    overflowY: 'auto'
  }
});

const Chatroom: React.FC<{
  sendMsg: (msg: any) => any;
  messages: Array<{ user: string; text: string; }>
}> = ({
  sendMsg,
  messages
}) => {
  const [messageToSend, setMsgToSend] = useState("");
  const classes = useStyles();
  const date = new Date()
  const userEmail = localStorage.getItem("userEmail") ?? "Anon";

  return (
    <>
      <Grid container>
        <Grid item xs={12}>
          <Typography variant="h5" className="header-message">Chat</Typography>
        </Grid>
      </Grid>
      <Grid container component={Paper} className={classes.chatSection}>
        <Grid item xs={12}>
          <List className={classes.messageArea}>
            {messages.map((msg, i) => (
              <ListItem key={i}>
                <Grid container>
                  <Grid item xs={12}>
                    <ListItemText primary={msg?.text}/>
                  </Grid>
                  <Grid item xs={12}>
                    <ListItemText secondary={`${msg?.user} at ${date.getHours()}:${date.getMinutes()}`}/>
                  </Grid>
                </Grid>
              </ListItem>
            ))}
          </List>
          <Divider/>
          <Grid container style={{ padding: '10px' }}>
            <Grid item xs={9}>
              <TextField
                id="outlined-basic-email"
                label="Type Something"
                fullWidth
                onChange={({ target: { value }}) => {
                  setMsgToSend(value);
                }}
              />
            </Grid>
            <Grid xs={3} alignItems="flex-end">
              <Fab
                color="primary"
                aria-label="add"
                onClick={() => {
                  sendMsg({ "user": userEmail, "text": messageToSend });
                }}
              >
                <SendIcon/>
              </Fab>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}

export default Chatroom;
