export const BASE_URL: string = 'http://localhost:8000';
export const BACKEND_URL: string = 'http://localhost:8000/api/v1';
export const POLL_INTERVAL: number = 1500; // ms
