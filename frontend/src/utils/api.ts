import { BACKEND_URL } from '../config';

export const apiGet = (url: string, options: RequestInit = {}) => {
  const token = localStorage.getItem('token');

  if (!options.headers) {
    options.headers = new Headers({
      Accept: 'application/json',
      Authorization: `Bearer ${token}`
    });
  }
  return fetch(url, options);
}

export const apiPost = (url: string, data: Record<string, unknown>, options: RequestInit = {}) => {
  const token = localStorage.getItem('token');

  if (!options.headers) {
    options.headers = new Headers({
      Accept: 'application/json',
      Authorization: `Bearer ${token}`
    });
  }
  return fetch(url, {
    ...options,
    method: "POST",
    body: JSON.stringify(data)
  });
}

export const getHelloWorldMsg = async () => {
  const response = await apiGet(BACKEND_URL);

  const data = await response.json();

  if (data.message) {
    return data.message;
  }

  return Promise.reject('Failed to get message from backend');
};
