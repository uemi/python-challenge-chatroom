import React, { FC, useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';

import { apiGet, apiPost, getHelloWorldMsg } from '../utils';
import { isAuthenticated } from '../utils';
import { BACKEND_URL, POLL_INTERVAL } from "../config";
import Chatroom from "../components/ChatRoom";

const useStyles = makeStyles((theme) => ({
  link: {
    color: '#61dafb',
  },
}));

export const Home: FC = () => {
  const [message, setMessage] = useState<string>('');
  const [messages, setMessages] = useState<{ user: string; text: string; }[]>([]);
  const [error, setError] = useState<string>('');
  const classes = useStyles();

  const sendMsg = (data: any) => {
    apiPost(`${BACKEND_URL}/chatroom/send_msg`, data).then(console.log);
  }

  const updateMessages = React.useCallback(() => {
    const resp = apiGet(`${BACKEND_URL}/chatroom/retrieve_msgs`);
    resp.then(r => {
      r.json().then(data => {
        if (data && data.messages) {
          const messages = data.messages.map(
            (msg: any) => {
              try {
                return JSON.parse(msg);
              } catch (e) {
                return null;
              }
            }
          );
          setMessages(messages.filter((v: any) => v !== null));
        }
      })
    });
  }, [setMessages]);

  React.useEffect(() => {
    const updateMsgs = () => {
      updateMessages();
      setTimeout(updateMsgs, POLL_INTERVAL);
    }
    updateMsgs();
  }, [updateMessages]);


  const queryBackend = async () => {
    try {
      const message = await getHelloWorldMsg();
      setMessage(message);
    } catch (err) {
      setError(err);
    }
  };

  return (
    <div style={{
      display: "flex",
      alignItems: "center",
      justifyContent: "space-around",
      width: "80%",
    }}>
      {isAuthenticated() ? (
        <span>
          <Chatroom sendMsg={sendMsg} messages={messages} />
        </span>
      ) : <></>}
      <span>
        {isAuthenticated() ? (
          <a className={classes.link} href="/logout">
            Logout
          </a>
        ) : (
          <>
            <a className={classes.link} href="/login">
              Login
            </a>
            <br/>
            <a className={classes.link} href="/signup">
              Sign Up
            </a>
          </>
        )}
        <br/>
        {!message && !error && (
          <a className={classes.link} href="#" onClick={() => queryBackend()}>
            Click to see whether you are authenticated
          </a>
        )}
        <br/>
        {message && (
          <p>
            The token is still active.
          </p>
        )}
        {error && (
          <p>
            The token has been expired. Please re-login.
          </p>
        )}
      </span>
    </div>
  );
};
