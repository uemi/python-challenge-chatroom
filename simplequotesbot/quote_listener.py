import redis
import time
import re
import csv
import requests

gen_stock_api = lambda c: f"https://stooq.com/q/l/?s={c}&f=sd2t2ohlcv&h&e=csv"
data_regex = re.compile(r".*data.*?b'(.+?)'")


def listen_for_quotes():
    r = redis.StrictRedis(host="redis", port=6379, db=0)
    p = r.pubsub(ignore_subscribe_messages=True)
    p.subscribe(["stock-msgs-channel"])

    while True:
        stock_code = p.get_message()
        if stock_code:
            print("New stock_code received.")
            d_match = data_regex.match(str(stock_code))
            if d_match:
                stock_code = d_match.group(1)

                with requests.Session() as s:
                    downloaded_csv = s.get(gen_stock_api(stock_code))
                    csv_content = downloaded_csv.content
                    if not csv_content:
                        return f"'{stock_code}' is not a valid stock code"

                    decoded = csv_content.decode('utf-8')
                    rows = list(csv.reader(decoded.splitlines(), delimiter=','))
                    if len(rows) == 2:
                        # Keys: Symbol, Date, Time, Open, High, Low, Close, Volume
                        as_dict = {k:v for k,v in zip(rows[0], rows[1])}
                        resp = f"{stock_code},{as_dict['Close']}"
                        r.publish("quotes-channel", resp)
        time.sleep(0.3)


if __name__ == '__main__':
    listen_for_quotes()
